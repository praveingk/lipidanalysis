

# Extract
# Pravein, 2020

__author__      = "Pravein Govindan Kannan"
__copyright__   = "Copyright 2017, NUS"

import re, sys
import os
import logging
import socket
import time
import pickle, pprint

if 3 > len(sys.argv):
    print 'Usage : extract <Input File> <Output File>'
    sys.exit(1)

# Initialize some basic parameter files
inputFile = str(sys.argv[1])
outputFile = str(sys.argv[2])
inF = open(inputFile, "r")
outF = open(outputFile, "w")
dataY = []; 
dataPair = {};
i = 0
#read first line
for line in inF.readlines():
    if i == 0:
        dataY = line.split(",")
        print dataY
    else :
        #print line
        lineSplit = line.split(",")
        dataX = lineSplit[0]
        for index in range(1,len(lineSplit)):
            line = dataX+","+dataY[index].rstrip()+","+lineSplit[index].rstrip()+"\n"
            outF.writelines(line)
    i = i+1
